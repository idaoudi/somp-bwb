/*
 * =====================================================================================
 *
 *       Filename:  main2.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  30/11/2020 13:37:22
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <stdlib.h>
#include <mutex>
#include <condition_variable>
#include <iostream>
#include <hwloc.h>
#include <thread>
#include <sched.h>
#include <vector>
#include <getopt.h>
#include <string>
#include <sstream>
#include <iostream>
#include <pthread.h>
#define ITER 2000

#define DOUBLE_BUFFERING 1
#define INSTRUCTION_PAUSE 1

std::string evict;
std::string BWmode;
std::string readersCpus;
std::string writersCpus;
int chunk = 1;
int nReaders, nWriters;
double sizeL1;
double sizeL2;
double sizeL3;
pthread_barrier_t barrier;
std::vector<double> elapsedTimeVec;

#include "headers/machine.hpp"
#include "headers/semaphore.hpp"
#include "headers/default.hpp"
#include "headers/half2.hpp"
#include "headers/thread.hpp"

int main (int argc, char* argv[]) {

    // HWLoc shit
    hwloc_topology_t topology;
    hwloc_topology_init(&topology);
    hwloc_topology_load(topology);

    // Get the machine attributes
    machineAttributes(topology);

    // Get arguments
    getArgs(argc, argv);

    //Parse the CPU lists
    std::vector<int> readersCpusVec = cpuListParser(readersCpus);
    std::vector<int> writersCpusVec = cpuListParser(writersCpus);

    //Get nReaders and nWriters
    nReaders = readersCpusVec.size();
    nWriters = writersCpusVec.size();

    // Create arrays with size ARRAY_SIZE
    const int ARRAY_SIZE = (sizeL3/sizeof(double))/chunk;

    std::vector<std::thread> threadsVector(nReaders+nWriters);
    std::vector<hwloc_obj_t> puVector;
    std::vector<std::string> modesVector;
    std::vector<double*> writersVector;
    std::vector<double*> writersVector2;
    std::vector<double*> readersVector;

    //Push back PUs
    for (int i = 0; i < nReaders; i++){
        hwloc_obj_t p = hwloc_get_obj_by_type(topology, HWLOC_OBJ_PU, readersCpusVec[i]);
        puVector.push_back(p);
    }
    int j = 0;
    for (int i = nReaders; i < nReaders+nWriters; i++){
        hwloc_obj_t p = hwloc_get_obj_by_type(topology, HWLOC_OBJ_PU, writersCpusVec[j]);
        puVector.push_back(p);
        j++;
    }

    //Push back arrays
    size_t size = ARRAY_SIZE*sizeof(double);
    for (int i = 0; i < nReaders; i++){
        //double* arr = (double*) malloc(ARRAY_SIZE*sizeof(double));
        double* arr = (double*) hwloc_alloc(topology, size);
        readersVector.push_back(arr);
    }
    for (int i = 0; i < nWriters; i++){
        //double* arr = (double*) malloc(ARRAY_SIZE*sizeof(double));
        double* arr = (double*) hwloc_alloc(topology, size);
        writersVector.push_back(arr);
    }
    for (int i = 0; i < nWriters; i++){
        //double* arr = (double*) malloc(ARRAY_SIZE*sizeof(double));
        double* arr = (double*) hwloc_alloc(topology, size);
        writersVector2.push_back(arr);
    }

    //Push back modes (readers first)
    for (int i = 0; i < nReaders; i++){
        modesVector.push_back("reader");
    }
    for (int i = nReaders; i < nReaders+nWriters; i++){
        modesVector.push_back("writer");
    }

    std::vector<Semaphore> semVec(nWriters);
    std::vector<Semaphore> semVec2(nWriters);
    for (unsigned int i = 0; i < semVec.size(); i++){
        semVec[i].set(0);
        semVec2[i].set(0);
    }
    
    elapsedTimeVec.reserve(nReaders);
    for (int i = 0; i < nReaders; i++){
        elapsedTimeVec.push_back(0.0);
    }
    //Push back threads and spawn
    int Scounter = 0;
     
    if (BWmode == "half2"){
        if (nReaders > 1){
            throw std::invalid_argument("You can't have multiple readers in ALLR mode\n");
        }
        if (modesVector[0] != "reader"){
            std::cout << "HUH....???" << std::endl;
        }
        
        //Unique reader first gets the whole vector of semaphores
        threadsVector.at(0) = std::thread(threadSpawnerHalf2R, std::ref(semVec), std::ref(semVec2), puVector[0], topology, readersVector[0], writersVector, ARRAY_SIZE, modesVector[0], std::ref(elapsedTimeVec[0]));
        //Then every writer takes his own semaphore
        for (int i = 1; i < nWriters+1; i++){
            threadsVector.at(i) = std::thread(threadSpawnerHalf2W, std::ref(semVec[i-1]), std::ref(semVec2[i-1]), puVector[i], topology, writersVector[i-1], ARRAY_SIZE, modesVector[i]);
        }
    }
    else if (BWmode == "default"){
        if (nReaders != nWriters){
            std::cout << nReaders << " - " << nWriters << std::endl;
            throw std::invalid_argument("Too much CPUs for this mode\n");
        }
        int j = 0;
        pthread_barrier_init (&barrier, NULL, nReaders);
        for (int i = 0; i < nReaders+nWriters; i++){
            threadsVector.at(i) = std::thread(threadSpawnerDefault, std::ref(semVec[j]), std::ref(semVec2[j]), puVector[i], topology, writersVector[j], writersVector2[j], readersVector[j], ARRAY_SIZE, modesVector[i], std::ref(elapsedTimeVec[j]));
            if (i == nReaders - 1){
                j = 0;
            }
            else {
                j++;
            }
        }
    }
        
    //Joining backwards
    for (int i = threadsVector.size()-1; i >= 0; i--){
        if (threadsVector.at(i).joinable()){
            threadsVector.at(i).join();}
    }
    pthread_barrier_destroy(&barrier);


    double bytes = ARRAY_SIZE*sizeof(double);
#ifdef DEBUG
    std::cout << "******* Cache to cache bandwidth *******" << std::endl;
    std::cout << "******* BW mode chosen: " << BWmode << "    *******" << std::endl;
    std::cout << "Array size (Mo) : " << ARRAY_SIZE*sizeof(double)/(1024*1024) << std::endl;
    std::cout << "Array size      : " << ARRAY_SIZE << std::endl;
    for (unsigned int i = 0; i < elapsedTimeVec.size(); i++){
        std::cout << "Measured time for Read N°" << i+1 << ": " << elapsedTimeVec[i]/ITER << "ms" << std::endl;
        std::cout << "Measured BW for Read N°  " << i+1 << ": " << (bytes*1e-9)/(elapsedTimeVec[i]/ITER) << "GBps" << std::endl;
    }
#else
    //	std::cout << "time, bw, chunk" << std::endl;
    //for (unsigned int i = 0; i < elapsedTimeVec.size(); i++){
    //    std::cout << elapsedTimeVec[i]/ITER << ", " << (bytes*1e-9)/(elapsedTimeVec[i]/ITER) << ", " << bytes << std::endl;
    //}
    std::cout << bytes;
    for (unsigned int i = 0; i < elapsedTimeVec.size(); i++){
        std::cout << ", ";
        std::cout << (bytes*1e-9)/(elapsedTimeVec[i]/ITER);
    }
    std::cout << std::endl;
#endif
}



