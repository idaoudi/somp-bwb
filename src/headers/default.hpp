void writerDefault(double tabW1[], double tabW2[], Semaphore& canRead, Semaphore& canWrite, const int ARRAY_SIZE, hwloc_obj_t pu) {
  //If evict mode is chosen
  if (evict == "yes"){
    for (int i = 0; i < ARRAY_SIZE; i++){
      tabW1[i] = 1.0;
      if (DOUBLE_BUFFERING == 1){
        tabW2[i] = 1.0;
      }
    }
  }
  for (unsigned int j = 0; j < ITER; j++){
    for (int i = 0; i < ARRAY_SIZE; i++){
      tabW1[i] = 11.0;
    }
    if (tabW1[0] != 11.0){
      std::cout << "Big problems (writer)" << std::endl;
    }
    //Notify reader
    canRead.notify(); 
    canWrite.wait();

    if (INSTRUCTION_PAUSE == 1){
    	for (volatile int i = 0; i < 10; i++){
	    asm("nop");
    	}
    }

    if (DOUBLE_BUFFERING == 1){
      for (int i = 0; i < ARRAY_SIZE; i++){
        tabW2[i] = 12.0;
      }
      if (tabW2[0] != 12.0){
        std::cout << "Big problems (writer)" << std::endl;
      }
      //Notify reader and wait for him to finish the previous array
      canRead.notify();
      canWrite.wait();
    }
  }
}

void readerDefault(double tabW1[], double tabW2[], double tabR[], Semaphore& canRead, Semaphore& canWrite, const int ARRAY_SIZE, hwloc_obj_t pu, double& elapsedTime) {
  if (evict == "yes"){
    for (int i = 0; i < ARRAY_SIZE; i++){
      tabR[i] = 2.0;
    }
  }
  if (DOUBLE_BUFFERING == 1){
    canWrite.notify();
  }
  for (int j = 0; j < ITER; j++){
    //Wait for writer to finish next array
    canRead.wait();
    pthread_barrier_wait (&barrier);

    auto begin1 = std::chrono::steady_clock::now();
    for (int i = 0; i < ARRAY_SIZE; i++){
      tabR[i] = tabW1[i];
    }
    __sync_synchronize();
    auto end1 = std::chrono::steady_clock::now();

    //Safety
    if (tabR[0] != 11.0){
      std::cout << "Big problems 1 (reader)" << std::endl;
      std::cout << "tabR: " << tabR[0] << std::endl;
    }
    //Notify writer and wait for next array
    canWrite.notify();
    if (DOUBLE_BUFFERING == 1){
      canRead.wait();
      pthread_barrier_wait (&barrier);

      auto begin2 = std::chrono::steady_clock::now();
      for (int i = 0; i < ARRAY_SIZE; i++){
        tabR[i] = tabW2[i];
      }
      __sync_synchronize();
      auto end2 = std::chrono::steady_clock::now();

      auto total1 = std::chrono::duration_cast<std::chrono::microseconds>(end1 - begin1).count() * 1e-6;
      auto total2 = std::chrono::duration_cast<std::chrono::microseconds>(end2 - begin2).count() * 1e-6;
      elapsedTime += (total1 + total2)/2;

      //Safety
      if (tabR[0] != 12.0){
        std::cout << "Big problems 2 (reader)" << std::endl;
        std::cout << "tabR: " << tabR[0] << std::endl;
      }
      canWrite.notify();
    }
    else {
      auto total1 = std::chrono::duration_cast<std::chrono::microseconds>(end1 - begin1).count() * 1e-6;
      elapsedTime += total1;
    }
  }
}
