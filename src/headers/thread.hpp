/*
 * =====================================================================================
 *
 *       Filename:  spawn.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  01/12/2020 15:52:38
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */


/* Default mode */
void threadSpawnerDefault(Semaphore& s, Semaphore& d, hwloc_obj_t pu0, hwloc_topology_t topology, double tabW1[], double tabW2[], double tabR[], const int ARRAY_SIZE, std::string mode, double& etime){
    // Bind to PU
    bind_to_pu(pu0, topology);
    if (mode == "reader"){
        readerDefault(tabW1, tabW2, tabR, s, d, ARRAY_SIZE, pu0, etime);
    }
    else if (mode == "writer"){
        writerDefault(tabW1, tabW2, s, d, ARRAY_SIZE, pu0);
    }
    else{
        throw std::invalid_argument("Problem in threadSpawnerW.\n");
    }
}

/* HALF2 mode */
void threadSpawnerHalf2R(std::vector<Semaphore>& semVec, std::vector<Semaphore>& semVec2, hwloc_obj_t pu, hwloc_topology_t topology, double tabR[], std::vector<double*> tabWVec, const int ARRAY_SIZE, std::string mode, double& etime){
    // Bind to PU
    bind_to_pu(pu, topology);
    if (mode == "writer"){
        throw std::invalid_argument("Something is not right in the thread spawner!\n");
    }
    else if (mode == "reader"){
        readerHalf2(tabR, tabWVec, std::ref(semVec), std::ref(semVec2), ARRAY_SIZE, pu, etime);
    }
}
void threadSpawnerHalf2W(Semaphore& s, Semaphore& g, hwloc_obj_t pu, hwloc_topology_t topology, double tabW[], const int ARRAY_SIZE, std::string mode){
    // Bind to PU
    bind_to_pu(pu, topology);
    if (mode == "reader"){
        throw std::invalid_argument("Something is not right in the thread spawner!\n");
    }
    else if (mode == "writer"){
        writerHalf2(tabW, std::ref(s), std::ref(g), ARRAY_SIZE, pu);
    }
}

/*  Cross mode */
//void threadSpawnerCross(Semaphore& s, hwloc_obj_t pu0, hwloc_topology_t topology, double tabR[], double tabW[], const int ARRAY_SIZE, std::string mode){
//    // Bind to PU
//    bind_to_pu(pu0, topology);
//    if (mode == "reader"){
//        readerCross(tabR, tabW, s, ARRAY_SIZE, pu0);
//    }
//    else if (mode == "writer"){
//        writerCross(tabW, s, ARRAY_SIZE, pu0);
//    }
//    else{
//        throw std::invalid_argument("Something is not right in the thread spawner SCR!\n");
//    }
//}
//
//void threadSpawnerVec(std::vector<Semaphore>& semVec, hwloc_obj_t pu0, hwloc_topology_t topology, double tabR[], double tabW[], const int ARRAY_SIZE, std::string mode){
//    // Bind to PU
//    bind_to_pu(pu0, topology);
//    if (mode == "writer"){
//        throw std::invalid_argument("Something is not right in the thread spawner!\n");
//    }
//    else if (mode == "reader"){
//        readerVec(tabR, tabW, std::ref(semVec), ARRAY_SIZE, pu0);
//    }
//}
