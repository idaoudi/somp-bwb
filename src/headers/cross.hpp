void writerCross(double tabW[], Semaphore& s, const int ARRAY_SIZE, hwloc_obj_t pu) {
    for (int i = 0; i < ARRAY_SIZE; i++){
        tabW[i] = 11.0;
    }
    //Safety
    if (tabW[0] != 11.0){
        std::cout << "Big problems (writer)" << std::endl;
    }
#ifdef DEBUG
    std::cout << "Writer finished " << pu->logical_index << std::endl;
#endif
    s.notify();
}

void readerCross(double tabR[], double tabW[], Semaphore& s, const int ARRAY_SIZE, hwloc_obj_t pu) {
    //If evict mode is chosen
    if (evict == "yes"){
        int sz = sizeL2/sizeof(double);
        double fakeArray[sz];
        for (int i = 0; i < sz; i++){
            fakeArray[i] = 1.0;
        }
    }
    //Mutlislot semaphore
    s.wait();
#ifdef DEBUG
    std::cout << "Reader finished waiting for all writers " << pu->logical_index << std::endl;
#endif
    auto begin = std::chrono::steady_clock::now();
    for (int j = 0; j < ITER; j++){
        for (int i = 0; i < ARRAY_SIZE; i++){
            tabR[i] = tabW[i];
        }
    }
    __sync_synchronize();
    auto end = std::chrono::steady_clock::now();
    //Safety
    if (tabW[0] != 11.0){
        std::cout << "Big problems (reader)" << std::endl;
        std::cout << "tabW: " << tabW[0] << std::endl;
    }
    elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() * 1e-6;
    elapsedTimeVec.push_back(elapsedTime);
    elapsedTime = 0.0;
}
