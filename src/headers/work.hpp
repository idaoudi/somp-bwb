void readerVec(double firstArray[], double secondArray[], std::vector<Semaphore>& semVec, const int ARRAY_SIZE, hwloc_obj_t pu) {
#ifdef DEBUG
    std::cout << "Reader waiting..." << std::endl;
#endif
    for (unsigned int i = 0; i < semVec.size(); i++){
        semVec[i].wait();
        //Start chrono 
        auto begin = std::chrono::steady_clock::now();
#ifdef DEBUG
        std::cout << "Thread " << pu->logical_index << " started reading..." << std::endl;
#endif
        for (int j = 0; j < ITER; j++){
            for (int i = 0; i < ARRAY_SIZE; i++){
                firstArray[i] = secondArray[i];
            }
        }
        //Safety
        if (secondArray[0] != 11.0){
            std::cout << "Big problems (reader)" << std::endl;
        }
        //End chrono
        auto end = std::chrono::steady_clock::now();
#ifdef DEBUG
        std::cout << "Thread " << pu->logical_index << " finished reading..." << std::endl;
#endif
        elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() * 1e-6;
    }
}


