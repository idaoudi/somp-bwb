/*
 * =====================================================================================
 *
 *       Filename:  machine.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  01/12/2020 17:19:24
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

void machineAttributes(hwloc_topology_t topology){
    //First core of the machine
    hwloc_obj_t pu0 = hwloc_get_obj_by_type(topology, HWLOC_OBJ_PU, 0);
    hwloc_obj_t core = pu0->parent;
    hwloc_obj_t cachel1 = core->parent;
    int ncores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_PU);
#ifdef DEBUG    
    std::cout << "******* Machine attributes *******" << std::endl;
    std::cout << "Number of cores: " << ncores << std::endl;
#endif
    if (hwloc_obj_type_is_cache(cachel1->type)){
        sizeL1 = cachel1->attr->cache.size;
#ifdef DEBUG    
        std::cout << "Cache L1 size  : " << sizeL1/1024 << "Ko" << std::endl;
#endif
        hwloc_obj_t cachel2 = cachel1->parent;
        sizeL2 = cachel2->attr->cache.size;
#ifdef DEBUG    
        std::cout << "Cache L2 size  : " << sizeL2/(1024*1024) << "Mo" << std::endl;
#endif
        hwloc_obj_t cachel3 = cachel2->parent;
        sizeL3 = cachel3->attr->cache.size;
#ifdef DEBUG    
        std::cout << "Cache L3 size  : " << sizeL3/(1024*1024) << "Mo" << std::endl;
#endif
    }
}

//void getArgs(int argc, char* argv[], std::string BWmode, std::string readersCpus, std::string writersCpus, int chunk, std::string evict){
void getArgs(int argc, char* argv[]){
    int arguments;
    const option longOptions[] = {
		{"mode", required_argument, nullptr, 'm'},
		{"readers_cpus", required_argument, nullptr, 'r'},
		{"writers_cpus", required_argument, nullptr, 'w'},
		{"chunk", required_argument, nullptr, 'c'},
		{"evict", required_argument, nullptr, 'e'},
		{nullptr, no_argument, nullptr, 0}
	};
	while ((arguments = getopt_long(argc, argv, "m:r:w:c:e:", longOptions, nullptr)) != -1){
		switch (arguments){
			case 'm':
				if (optarg) BWmode = optarg;
				break;
			case 'r':
				if (optarg) readersCpus = optarg;
				break;
			case 'w':
				if (optarg) writersCpus = optarg;
				break;
			case 'c':
				if (optarg) chunk = atoi(optarg);
				break;
            case 'e':
                if (optarg) evict = optarg;
                break;
			case '?':
				throw std::invalid_argument("Invalid parameters.");
				//FIXME help()
		}
	}
}

std::vector<int> cpuListParser(std::string expression){
    std::vector<int> cpus;
    std::stringstream ss(expression);
    while (ss.good()){
        std::string tmp;
        std::getline(ss, tmp, ',');
        cpus.push_back(stoi(tmp));
    }
    return cpus;
}

void bind_to_pu(hwloc_obj_t pu, hwloc_topology_t topo){
    if (hwloc_set_cpubind(topo, pu->cpuset, HWLOC_CPUBIND_THREAD) < 0){
        std::cerr << "Error with hwloc_set_cpubind!" << std::endl;
        std::exit(1);
    }
}

