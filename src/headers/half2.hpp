void writerHalf2(double tabW[], Semaphore& s, Semaphore& g, const int ARRAY_SIZE, hwloc_obj_t pu){
    for (int j = 0; j < ITER; j++){
        //First half
        for (int i = 0; i < ARRAY_SIZE/2; i++){
            tabW[i] = 21.0;
        }	
        s.notify();
        g.wait();

        //Second half
        for (int i = ARRAY_SIZE/2; i < ARRAY_SIZE; i++){
            tabW[i] = 21.0;
        }
        s.notify();
        g.wait();
    }
}

void readerHalf2(double tabR[], std::vector<double*> tabWVec, std::vector<Semaphore>& semVec, std::vector<Semaphore>& semVec2, const int ARRAY_SIZE, hwloc_obj_t pu, double& elapsedTime){
    //If evict mode is chosen
    if (evict == "yes"){
        int sz = sizeL2/sizeof(double);
        double fakeArray[sz];
        for (int i = 0; i < sz; i++){
            fakeArray[i] = 1.0;
        }
    }

    double total1 = 0.0;
    double total2 = 0.0;
    for (int j = 0; j < ITER; j++){
        int cnt = 0;
        //First halfs
        for (auto tab: tabWVec){
            semVec[cnt].wait();
            auto begin1 = std::chrono::steady_clock::now();
            for (int i = 0; i < ARRAY_SIZE/2; i++){
                tabR[i] = tab[i];
            }
            __sync_synchronize();
            auto end1 = std::chrono::steady_clock::now();
            total1 = std::chrono::duration_cast<std::chrono::microseconds>(end1 - begin1).count() * 1e-6;
            semVec2[cnt].notify();
            cnt++;
        }

        cnt = 0;
        //Second halfs
        for (auto tab: tabWVec){
            semVec[cnt].wait();
            auto begin2 = std::chrono::steady_clock::now();
            for (int i = ARRAY_SIZE/2; i < ARRAY_SIZE; i++){
                tabR[i] = tab[i];
            }
            __sync_synchronize();
            auto end2 = std::chrono::steady_clock::now();
            total2 = std::chrono::duration_cast<std::chrono::microseconds>(end2 - begin2).count() * 1e-6;
            semVec2[cnt].notify();
            cnt++;
        }
        elapsedTime += total1 + total2;
    }
}
