class Semaphore {
    private:
        std::mutex iomutex;
        std::condition_variable cv;
        int counter = 0; //Locked by default
        int immuneCounter;
    public:
        void set (int c){
            counter = c;
            immuneCounter = c;
        }
        void handle() {
            std::cout << iomutex.native_handle() << std::endl;
        }
        void wait() {
            std::unique_lock<std::mutex> ulock(iomutex);
            if (immuneCounter == 0){
                while (counter == 0){
                    cv.wait(ulock);
                }
                counter--;
            }
            else {
                while (counter < 0){
                    cv.wait(ulock);
                }   
            }
        }
        void notify() {
            std::lock_guard<std::mutex> glock(iomutex);
            counter++;
            if (immuneCounter == 0){
                cv.notify_one();
            }
            else {
               cv.notify_all(); 
            }
        }
};


