#ARG1 architecture
#ARG2 mode
#ARG3 socket/die 

import matplotlib as mpl
import glob
import numpy as np
import matplotlib.pyplot as plt
import sys
import re
from colour import Color
from matplotlib.pyplot import cm

if (sys.argv[1] == "amd"):
    res = "results/amd/"
    title1 = "Bandwidth measures on AMD EPYC 7452\n"
elif (sys.argv[1] == "intel"):
    res = "results/intel/"
    title1 = "Bandwidth measures on Intel Xeon Gold 6240\n"
else:
    sys.exit()

mode     = sys.argv[2]
sd       = sys.argv[3]
dash     = "_"
wildcard = "*/*.csv"

directory = res + mode + dash + sd + wildcard
files = sorted(glob.glob(directory))

for f in files:
    print(f)

plt.rcParams.update({'font.size': 12})
fig = plt.figure(figsize=(15,10))
ax1 = fig.add_subplot(111)
allcolors = []
var = 0
signs = iter({'x', 'o', 'v', '<', '*', '.', '+', 'D'})

if (mode == "crossmulti"):
    colors = iter(cm.rainbow(np.linspace(0,1,33)))
else:
    colors = iter(cm.rainbow(np.linspace(0,1,int(len(files)))))

cnt = 0
j = 0
for f in files:
    string = f
    legend = string[len(res)+len(mode)+1:len(string)-4]
    if (cnt%18 == 0):
        if (mode == "default"):
            colors = iter(cm.rainbow(np.linspace(0,1,int(len(files)/2))))
        sig = next(signs)
    col = next(colors)
    for i in range(4):
        tmp = legend.find('-')
        legend = legend[tmp+1:len(legend)]
    if (mode == "cross"):
        legend1 = str("First to ") + str(legend)
        legend2 = str("Second to ") + str(legend)
        data = np.genfromtxt(f, delimiter=', ', names=['bytes', 'bw', 'bw2'], loose=False, invalid_raise=False)
        ax1.plot(data['bytes'], data['bw'], '.-', label='%s'%legend1, color=col)
        ax1.plot(data['bytes'], data['bw2'], '*-', label='%s'%legend2, color=col)
    elif (mode == "crossmulti"):
        #data = np.genfromtxt(f, delimiter=', ', names=['bytes', 'bw1', 'bw2', 'bw3', 'bw4', 'bw5', 'bw6', 'bw7', 'bw8'], loose=False, invalid_raise=False)
        data = np.genfromtxt(f, delimiter=', ', names=['bytes', 'bw1', 'bw2', 'bw3', 'bw4', 'bw5', 'bw6', 'bw7', 'bw8', 'bw9', 'bw10', 'bw11', 'bw12', 'bw13', 'bw14', 'bw15', 'bw16', 'bw17', 'bw18', 'bw19', 'bw20', 'bw21', 'bw22', 'bw23', 'bw24', 'bw25', 'bw26', 'bw27', 'bw28', 'bw29', 'bw30', 'bw31', 'bw32'], loose=False, invalid_raise=False)
        #, 'bw19', 'bw20', 'bw21', 'bw22', 'bw23', 'bw24', 'bw25', 'bw26', 'bw27', 'bw28', 'bw29', 'bw30', 'bw31', 'bw32'], loose=False, invalid_raise=False)
        ax1.plot(data['bytes'], data['bw1'], '*-', label='0-18' ,color=col)
        ax1.plot(data['bytes'], data['bw2'], '*-', label='1-19' ,color=next(colors))
        ax1.plot(data['bytes'], data['bw3'], '*-', label='2-20' ,color=next(colors))
        ax1.plot(data['bytes'], data['bw4'], '*-', label='3-21' ,color=next(colors))
        ax1.plot(data['bytes'], data['bw5'], '*-', label='4-22' ,color=next(colors))
        ax1.plot(data['bytes'], data['bw6'], '*-', label='5-23' ,color=next(colors))
        ax1.plot(data['bytes'], data['bw7'], '*-', label='6-24' ,color=next(colors))
        ax1.plot(data['bytes'], data['bw8'], '*-', label='7-25' ,color=next(colors))
        ax1.plot(data['bytes'], data['bw9'], '*-', label='8-26' ,color=next(colors))
        ax1.plot(data['bytes'], data['bw10'], '*-',label='9-27' , color=next(colors))
        ax1.plot(data['bytes'], data['bw11'], '*-',label='10-28' , color=next(colors))
        ax1.plot(data['bytes'], data['bw12'], '*-',label='11-29' , color=next(colors))
        ax1.plot(data['bytes'], data['bw13'], '*-',label='12-30' , color=next(colors))
        ax1.plot(data['bytes'], data['bw14'], '*-',label='13-31' , color=next(colors))
        ax1.plot(data['bytes'], data['bw15'], '*-',label='14-32' , color=next(colors))
        ax1.plot(data['bytes'], data['bw16'], '*-',label='15-33' , color=next(colors))
        ax1.plot(data['bytes'], data['bw17'], '*-',label='16-34' , color=next(colors))
        ax1.plot(data['bytes'], data['bw18'], '*-',label='17-35' , color=next(colors))
        ax1.plot(data['bytes'], data['bw19'], '*-',label='18-50' , color=next(colors))
        ax1.plot(data['bytes'], data['bw20'], '*-',label='19-51' , color=next(colors))
        ax1.plot(data['bytes'], data['bw21'], '*-',label='20-52' , color=next(colors))
        ax1.plot(data['bytes'], data['bw22'], '*-',label='21-53' , color=next(colors))
        ax1.plot(data['bytes'], data['bw23'], '*-',label='22-54' , color=next(colors))
        ax1.plot(data['bytes'], data['bw24'], '*-',label='23-55' , color=next(colors))
        ax1.plot(data['bytes'], data['bw25'], '*-',label='24-56' , color=next(colors))
        ax1.plot(data['bytes'], data['bw26'], '*-',label='25-57' , color=next(colors))
        ax1.plot(data['bytes'], data['bw27'], '*-',label='26-58' , color=next(colors))
        ax1.plot(data['bytes'], data['bw28'], '*-',label='27-59' , color=next(colors))
        ax1.plot(data['bytes'], data['bw29'], '*-',label='28-60' , color=next(colors))
        ax1.plot(data['bytes'], data['bw30'], '*-',label='29-61' , color=next(colors))
        ax1.plot(data['bytes'], data['bw31'], '*-',label='30-62' , color=next(colors))
        ax1.plot(data['bytes'], data['bw32'], '*-',label='30-62' , color=next(colors))
        ax1.plot(data['bytes'], data['bw1']+data['bw2']+data['bw3']+data['bw4']+data['bw5']+data['bw6']+data['bw7']+data['bw8']+data['bw9']+data['bw10']+data['bw11']+data['bw12']+data['bw13']+data['bw14']+data['bw15']+data['bw16']+data['bw17']+data['bw18']+data['bw19']+data['bw20']+data['bw21']+data['bw22']+data['bw23']+data['bw24']+data['bw25']+data['bw26']+data['bw27']+data['bw28']+data['bw29']+data['bw30']+data['bw31']+data['bw32'], 'o-',label='Sum of BWs' , color='r')
    else:
        data = np.genfromtxt(f, delimiter=', ', names=['bytes', 'bw'], invalid_raise=False)
        ax1.plot(data['bytes'], data['bw'], sig, label='%s'%legend, color=col)
    cnt += 1

if (mode == "crossmulti"):
    ax1.legend(ncol=3, loc='upper right')
else:
    ax1.legend(ncol=int(len(files)/16)+2, loc='upper right')

plt.ylim([0,70])
plt.yticks(np.arange(0, 70, step=5))
for i in range(0,70,5):
    plt.axhline(y=i, xmin=0, xmax=20, linewidth=0.2, color='black') 
plt.xscale("log")
mode = mode[0].upper() + mode[1:len(mode)]    
title2 = mode + " mode"
ax1.set_title(title1+title2)
ax1.set_xlabel('Data (B)')
ax1.set_ylabel('Bandwidth (GBps)')
filename = sys.argv[1] + "-" + mode + "-fullsocket.eps"
plt.savefig(filename, dpi=1200, format='eps')
plt.show()
