import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.pyplot import cm
from colour import Color
import sys

arch = sys.argv[1]
f = sys.argv[2]
var = sys.argv[3]
var = int(var)
die = sys.argv[4]

title = "Bandwidth measures on Intel Xeon Gold 6240\n"
#title = "Bandwidth measures on AMD EPYC 7452\n"

plt.rcParams.update({'font.size': 12})
fig = plt.figure(figsize=(15,10))
ax1 = fig.add_subplot(111)
colors = iter(cm.rainbow(np.linspace(0,1,var+1)))

a = sys.argv[5]
u = sys.argv[9]
legend1 = "R:CPU"+u+" - W:CPU"+a
#+" & CPU"+b
#& CPU63"
b = sys.argv[6]
c = sys.argv[7]
d = sys.argv[8]
v = sys.argv[10]
w = sys.argv[11]
x = sys.argv[12]
a1 = sys.argv[13]
a2 = sys.argv[14]
a3 = sys.argv[15]
a4 = sys.argv[16]
x1 = sys.argv[17]
x2 = sys.argv[18]
x3 = sys.argv[19]
x4 = sys.argv[20]
legend2 = "R:CPU"+v+" - W:CPU"+b
legend3 = "R:CPU"+w+" - W:CPU"+c
legend4 = "R:CPU"+x+" - W:CPU"+d
legend5 = "R:CPU"+x1+" - W:CPU"+a1
legend6 = "R:CPU"+x2+" - W:CPU"+a2
legend7 = "R:CPU"+x3+" - W:CPU"+a3
legend8 = "R:CPU"+x4+" - W:CPU"+a4

if (var == 1):
    data = np.genfromtxt(f, delimiter=', ', names=['bytes', 'bw'], loose=False, invalid_raise=False)
elif (var == 2):
    data = np.genfromtxt(f, delimiter=', ', names=['bytes', 'bw', 'bw2'], loose=False, invalid_raise=False)
elif (var == 3):
    data = np.genfromtxt(f, delimiter=', ', names=['bytes', 'bw', 'bw2', 'bw3'], loose=False, invalid_raise=False)
elif (var == 4):
    data = np.genfromtxt(f, delimiter=', ', names=['bytes', 'bw', 'bw2', 'bw3', 'bw4'], loose=False, invalid_raise=False)
elif (var == 5):
    data = np.genfromtxt(f, delimiter=', ', names=['bytes', 'bw', 'bw2', 'bw3', 'bw4', 'bw5'], loose=False, invalid_raise=False)
elif (var == 6):
    data = np.genfromtxt(f, delimiter=', ', names=['bytes', 'bw', 'bw2', 'bw3', 'bw4', 'bw5', 'bw6'], loose=False, invalid_raise=False)
elif (var == 7):
    data = np.genfromtxt(f, delimiter=', ', names=['bytes', 'bw', 'bw2', 'bw3', 'bw4', 'bw5', 'bw6', 'bw7'], loose=False, invalid_raise=False)
elif (var == 8):
    data = np.genfromtxt(f, delimiter=', ', names=['bytes', 'bw', 'bw2', 'bw3', 'bw4', 'bw5', 'bw6', 'bw7', 'bw8'], loose=False, invalid_raise=False)

legendsum = "Sum of BWs"

if (var == 1):
    ax1.plot(data['bytes']*var, data['bw'], '.-', label='%s'%legend1, color=next(colors))
elif (var == 2):
    ax1.plot(data['bytes']*var, data['bw'], '.-', label='%s'%legend1, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw2'], 'o-', label='%s'%legend2, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw'] + data['bw2'], '*-', label='%s'%legendsum, color='r')
elif (var == 3):
    ax1.plot(data['bytes']*var, data['bw'], '.-', label='%s'%legend1, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw2'], 'o-', label='%s'%legend2, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw3'], '*-', label='%s'%legend3, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw'] + data['bw2'] + data['bw3'], '*-', label='%s'%legendsum, color='r')
elif (var == 4):
    ax1.plot(data['bytes']*var, data['bw'], '.-', label='%s'%legend1, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw2'], 'o-', label='%s'%legend2, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw3'], '*-', label='%s'%legend3, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw4'], '>-', label='%s'%legend4, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw'] + data['bw2'] + data['bw3'] + data['bw4'], '*-', label='%s'%legendsum, color='r')
elif (var == 5):
    ax1.plot(data['bytes']*var, data['bw'], '.-', label='%s'%legend1, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw2'], 'o-', label='%s'%legend2, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw3'], '*-', label='%s'%legend3, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw4'], '>-', label='%s'%legend4, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw5'], '<-', label='%s'%legend5, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw'] + data['bw2'] + data['bw3'] + data['bw4'] + data['bw5'], '*-', label='%s'%legendsum, color='r')
elif (var == 6):
    ax1.plot(data['bytes']*var, data['bw'], '.-', label='%s'%legend1, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw2'], 'o-', label='%s'%legend2, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw3'], '*-', label='%s'%legend3, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw4'], '>-', label='%s'%legend4, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw5'], '<-', label='%s'%legend5, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw6'], 'd-', label='%s'%legend6, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw'] + data['bw2'] + data['bw3'] + data['bw4'] + data['bw5'] + data['bw6'], '*-', label='%s'%legendsum, color='r')
elif (var == 7):
    ax1.plot(data['bytes']*var, data['bw'], '.-', label='%s'%legend1, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw2'], 'o-', label='%s'%legend2, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw3'], '*-', label='%s'%legend3, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw4'], '>-', label='%s'%legend4, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw5'], '<-', label='%s'%legend5, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw6'], 'd-', label='%s'%legend6, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw7'], 'p-', label='%s'%legend7, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw'] + data['bw2'] + data['bw3'] + data['bw4'] + data['bw5'] + data['bw6'] + data['bw7'], '*-', label='%s'%legendsum, color='r')
elif (var == 8):
    ax1.plot(data['bytes']*var, data['bw'], '.-', label='%s'%legend1, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw2'], 'o-', label='%s'%legend2, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw3'], '*-', label='%s'%legend3, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw4'], '>-', label='%s'%legend4, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw5'], '<-', label='%s'%legend5, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw6'], 'd-', label='%s'%legend6, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw7'], 'p-', label='%s'%legend7, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw8'], 'h-', label='%s'%legend8, color=next(colors))
    ax1.plot(data['bytes']*var, data['bw'] + data['bw2'] + data['bw3'] + data['bw4'] + data['bw5'] + data['bw6'] + data['bw7'] + data['bw8'], '*-', label='%s'%legendsum, color='r')


ax1.legend(ncol=1, loc='upper right')
plt.ylim([0,50])
plt.yticks(np.arange(0, 50, step=5))
for i in range(0,50,5):
    plt.axhline(y=i, xmin=0, xmax=50, linewidth=0.2, color='black') 
plt.xscale("log")
ax1.set_xlabel('Data (B)')
ax1.set_ylabel('Bandwidth (GBps)')
#title2 = "Cross mode: Socket 0 - Socket "+die+" / Socket 1 - Socket 0"
#title2 = "Half2 mode"
title2 = "Cross mode: DIE0 - DIE9 / DIE1 - DIE8"
ax1.set_title(title+title2)
#filename = sys.argv[1] + "-cross"+str(var)+"-die"+die+".eps"
filename = sys.argv[1] + "-newmisc-case"+die+".eps"
#filename = sys.argv[1] + "-half2-case"+die+".eps"
plt.savefig(filename, dpi=1200, format='eps')
#plt.show()






