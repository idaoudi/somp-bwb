#gnuplotfile

set title "Mesure des bandes passantes avec sOMP-BWB - AMD EPYC 7452\n ".ARG1." paires lecteur/écrivain dans des sens contraires entre die 0 et die 1"
set xlabel "Données (Bytes)"
set ylabel "Bande passante (GBps)"


set logscale x
set yrange[1:20]

set key top right vertical maxrows 16
set style data points
set terminal qt size 1000,800 font "Helvetica,22"
set key font ",17"

if (ARG1 eq "2"){
	plot 'amd/shared/final-amd-misc-evict-shared-2.csv' using 1:2 ps 2 pt 1 lw 3 lc rgb "red" title 'R:CPU0 - W:CPU4',\
		'amd/shared/final-amd-misc-evict-shared-2.csv' using 1:3 ps 2 pt 1 lw 3 lc rgb "orange" title 'R:CPU5 - W:CPU1'
}

if (ARG1 eq "4"){
	plot 'amd/shared/final-amd-misc-evict-shared-4.csv' using 1:2 ps 2 pt 1 lw 3 lc rgb "red" title 'R:CPU0 - W:CPU4',\
		'amd/shared/final-amd-misc-evict-shared-4.csv' using 1:3 ps 2 pt 1 lw 3 lc rgb "orange" title 'R:CPU5 - W:CPU1',\
		'amd/shared/final-amd-misc-evict-shared-4.csv' using 1:4 ps 2 pt 1 lw 3 lc rgb "cyan" title 'R:CPU2 - W:CPU6',\
		'amd/shared/final-amd-misc-evict-shared-4.csv' using 1:5 ps 2 pt 1 lw 3 lc rgb "blue" title 'R:CPU7 - W:CPU3',\
		'amd/shared/final-amd-misc-evict-shared-4-sum.csv' using 1:2 ps 2 pt 2 lw 3 lc rgb "green" title 'Somme des bandes passantes dans le sens die 0 -> die 1',\
		'amd/shared/final-amd-misc-evict-shared-4-sum.csv' using 1:3 ps 2 pt 2 lw 3 lc rgb "dark-green" title 'Somme des bandes passantes dans le sens die 1 -> die 0'
}
