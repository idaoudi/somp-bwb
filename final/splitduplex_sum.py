import sys
import numpy as np

f = sys.argv[1]
var = sys.argv[2]
var = int(var)

sm = []
if (var == 2):
    data = np.genfromtxt(f, delimiter=', ', names=['bytes', 'bw', 'bw2'], loose=False, invalid_raise=False)
    sm = data['bw'] + data['bw2']
    for i in range(len(data['bytes'])):
        print(data['bytes'][i], sm[i])
elif (var == 3):
    data = np.genfromtxt(f, delimiter=', ', names=['bytes', 'bw', 'bw2', 'bw3'], loose=False, invalid_raise=False)
    sm = data['bw'] + data['bw2'] + data['bw3']
    for i in range(len(data['bytes'])):
        print(data['bytes'][i], sm[i])
elif (var == 4):
    data = np.genfromtxt(f, delimiter=', ', names=['bytes', 'bw', 'bw2', 'bw3', 'bw4'], loose=False, invalid_raise=False)
    sm = data['bw'] + data['bw2'] + data['bw3'] + data['bw4']
    for i in range(len(data['bytes'])):
        print(data['bytes'][i], sm[i])
else:
    sys.exit("Error var")







