import sys
import numpy as np

f = sys.argv[1]

sm1 = []
sm2 = []
data = np.genfromtxt(f, delimiter=', ', names=['bytes', 'bw', 'bw2', 'bw3', 'bw4'], loose=False, invalid_raise=False)
sm1 = data['bw'] + data['bw3']
sm2 = data['bw2'] + data['bw4']
for i in range(len(data['bytes'])):
    print(data['bytes'][i], sm1[i], sm2[i])

