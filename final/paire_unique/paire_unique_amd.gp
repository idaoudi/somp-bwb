#gnuplotfile

set title "Mesure des bandes passantes avec sOMP-BWB - AMD EPYC 7452\n Paire unique lecteur/écrivain - Socket 1"
set xlabel "Données (Bytes)"
set ylabel "Bande passante (GBps)"

set yrange[1:10000]

set logscale
set key top right vertical maxrows 16
set style data points
set terminal qt size 1000,800 font "Helvetica,22"
set key font ",17"

#plot 'amd/default_socket/final-amd-default-evict-0-0.csv' ps 2 pt 1 lw 3 lc rgb "red" title 'R:CPU0 - W:CPU0',\
#     'amd/default_socket/final-amd-default-evict-0-01.csv' ps 2 pt 1 lw 3 lc rgb "orange" title 'R:CPU0 - W:CPU1',\
#     'amd/default_socket/final-amd-default-evict-0-02.csv' ps 2 pt 1 lw 3 lc rgb "yellow" title 'R:CPU0 - W:CPU2',\
#     'amd/default_socket/final-amd-default-evict-0-03.csv' ps 2 pt 1 lw 3 lc rgb "green" title 'R:CPU0 - W:CPU3',\
#     'amd/default_socket/final-amd-default-evict-0-04.csv' ps 2 pt 2 lw 3 lc rgb "cyan" title 'R:CPU0 - W:CPU4',\
#     'amd/default_socket/final-amd-default-evict-0-05.csv' ps 2 pt 2 lw 3 lc rgb "blue" title 'R:CPU0 - W:CPU5',\
#     'amd/default_socket/final-amd-default-evict-0-06.csv' ps 2 pt 2 lw 3 lc rgb "violet" title 'R:CPU0 - W:CPU6',\
#     'amd/default_socket/final-amd-default-evict-0-07.csv' ps 2 pt 2 lw 3 lc rgb "black" title 'R:CPU0 - W:CPU7',\
#	  'amd/default_socket/final-amd-default-evict-0-08.csv' ps 2 pt 3 lw 3 lc rgb "red" title 'R:CPU0 - W:CPU8',\
#	  'amd/default_socket/final-amd-default-evict-0-09.csv' ps 2 pt 3 lw 3 lc rgb "orange" title 'R:CPU0 - W:CPU9',\
#	  'amd/default_socket/final-amd-default-evict-0-10.csv' ps 2 pt 3 lw 3 lc rgb "yellow" title 'R:CPU0 - W:CPU10',\
#	  'amd/default_socket/final-amd-default-evict-0-11.csv' ps 2 pt 3 lw 3 lc rgb "green" title 'R:CPU0 - W:CPU11',\
#	  'amd/default_socket/final-amd-default-evict-0-12.csv' ps 2 pt 4 lw 3 lc rgb "cyan" title 'R:CPU0 - W:CPU12',\
#	  'amd/default_socket/final-amd-default-evict-0-13.csv' ps 2 pt 4 lw 3 lc rgb "blue" title 'R:CPU0 - W:CPU13',\
#	  'amd/default_socket/final-amd-default-evict-0-14.csv' ps 2 pt 4 lw 3 lc rgb "violet" title 'R:CPU0 - W:CPU14',\
#	  'amd/default_socket/final-amd-default-evict-0-15.csv' ps 2 pt 4 lw 3 lc rgb "black" title 'R:CPU0 - W:CPU15',\
#     'amd/default_socket/final-amd-default-evict-0-16.csv' ps 2 pt 5 lw 3 lc rgb "red" title 'R:CPU0 - W:CPU16',\
#	  'amd/default_socket/final-amd-default-evict-0-17.csv' ps 2 pt 5 lw 3 lc rgb "orange" title 'R:CPU0 - W:CPU17',\
#	  'amd/default_socket/final-amd-default-evict-0-18.csv' ps 2 pt 5 lw 3 lc rgb "yellow" title 'R:CPU0 - W:CPU18',\
#	  'amd/default_socket/final-amd-default-evict-0-19.csv' ps 2 pt 5 lw 3 lc rgb "green" title 'R:CPU0 - W:CPU19',\
#	  'amd/default_socket/final-amd-default-evict-0-20.csv' ps 2 pt 6 lw 3 lc rgb "cyan" title 'R:CPU0 - W:CPU20',\
#	  'amd/default_socket/final-amd-default-evict-0-21.csv' ps 2 pt 6 lw 3 lc rgb "blue" title 'R:CPU0 - W:CPU21',\
#	  'amd/default_socket/final-amd-default-evict-0-22.csv' ps 2 pt 6 lw 3 lc rgb "violet" title 'R:CPU0 - W:CPU22',\
#	  'amd/default_socket/final-amd-default-evict-0-23.csv' ps 2 pt 6 lw 3 lc rgb "black" title 'R:CPU0 - W:CPU23',\
#     'amd/default_socket/final-amd-default-evict-0-24.csv' ps 2 pt 7 lw 3 lc rgb "red" title 'R:CPU0 - W:CPU24',\
#	  'amd/default_socket/final-amd-default-evict-0-25.csv' ps 2 pt 7 lw 3 lc rgb "orange" title 'R:CPU0 - W:CPU25',\
#	  'amd/default_socket/final-amd-default-evict-0-26.csv' ps 2 pt 7 lw 3 lc rgb "yellow" title 'R:CPU0 - W:CPU26',\
#	  'amd/default_socket/final-amd-default-evict-0-27.csv' ps 2 pt 7 lw 3 lc rgb "green" title 'R:CPU0 - W:CPU27',\
#	  'amd/default_socket/final-amd-default-evict-0-28.csv' ps 2 pt 8 lw 3 lc rgb "cyan" title 'R:CPU0 - W:CPU28',\
#	  'amd/default_socket/final-amd-default-evict-0-29.csv' ps 2 pt 8 lw 3 lc rgb "blue" title 'R:CPU0 - W:CPU29',\
#	  'amd/default_socket/final-amd-default-evict-0-30.csv' ps 2 pt 8 lw 3 lc rgb "violet" title 'R:CPU0 - W:CPU30',\
#	  'amd/default_socket/final-amd-default-evict-0-31.csv' ps 2 pt 8 lw 3 lc rgb "black" title 'R:CPU0 - W:CPU31',\

plot 'amd/default_socket/final-amd-default-evict-0-32.csv' ps 2 pt 1 lw 3 lc rgb "red" title 'R:CPU0 - W:CPU32',\
     'amd/default_socket/final-amd-default-evict-0-33.csv' ps 2 pt 1 lw 3 lc rgb "orange" title 'R:CPU0 - W:CPU33',\
     'amd/default_socket/final-amd-default-evict-0-34.csv' ps 2 pt 1 lw 3 lc rgb "yellow" title 'R:CPU0 - W:CPU34',\
     'amd/default_socket/final-amd-default-evict-0-35.csv' ps 2 pt 1 lw 3 lc rgb "green" title 'R:CPU0 - W:CPU35',\
     'amd/default_socket/final-amd-default-evict-0-36.csv' ps 2 pt 2 lw 3 lc rgb "cyan" title 'R:CPU0 - W:CPU36',\
     'amd/default_socket/final-amd-default-evict-0-37.csv' ps 2 pt 2 lw 3 lc rgb "blue" title 'R:CPU0 - W:CPU37',\
     'amd/default_socket/final-amd-default-evict-0-38.csv' ps 2 pt 2 lw 3 lc rgb "violet" title 'R:CPU0 - W:CPU38',\
     'amd/default_socket/final-amd-default-evict-0-39.csv' ps 2 pt 2 lw 3 lc rgb "black" title 'R:CPU0 - W:CPU39',\
     'amd/default_socket/final-amd-default-evict-0-40.csv' ps 2 pt 3 lw 3 lc rgb "red" title 'R:CPU0 - W:CPU40',\
	  'amd/default_socket/final-amd-default-evict-0-41.csv' ps 2 pt 3 lw 3 lc rgb "orange" title 'R:CPU0 - W:CPU41',\
	  'amd/default_socket/final-amd-default-evict-0-42.csv' ps 2 pt 3 lw 3 lc rgb "yellow" title 'R:CPU0 - W:CPU42',\
	  'amd/default_socket/final-amd-default-evict-0-43.csv' ps 2 pt 3 lw 3 lc rgb "green" title 'R:CPU0 - W:CPU43',\
	  'amd/default_socket/final-amd-default-evict-0-44.csv' ps 2 pt 4 lw 3 lc rgb "cyan" title 'R:CPU0 - W:CPU44',\
	  'amd/default_socket/final-amd-default-evict-0-45.csv' ps 2 pt 4 lw 3 lc rgb "blue" title 'R:CPU0 - W:CPU45',\
	  'amd/default_socket/final-amd-default-evict-0-46.csv' ps 2 pt 4 lw 3 lc rgb "violet" title 'R:CPU0 - W:CPU46',\
	  'amd/default_socket/final-amd-default-evict-0-47.csv' ps 2 pt 4 lw 3 lc rgb "black" title 'R:CPU0 - W:CPU47',\
     'amd/default_socket/final-amd-default-evict-0-48.csv' ps 2 pt 5 lw 3 lc rgb "red" title 'R:CPU0 - W:CPU48',\
	  'amd/default_socket/final-amd-default-evict-0-49.csv' ps 2 pt 5 lw 3 lc rgb "orange" title 'R:CPU0 - W:CPU49',\
	  'amd/default_socket/final-amd-default-evict-0-50.csv' ps 2 pt 5 lw 3 lc rgb "yellow" title 'R:CPU0 - W:CPU50',\
	  'amd/default_socket/final-amd-default-evict-0-51.csv' ps 2 pt 5 lw 3 lc rgb "green" title 'R:CPU0 - W:CPU51',\
	  'amd/default_socket/final-amd-default-evict-0-52.csv' ps 2 pt 6 lw 3 lc rgb "cyan" title 'R:CPU0 - W:CPU52',\
	  'amd/default_socket/final-amd-default-evict-0-53.csv' ps 2 pt 6 lw 3 lc rgb "blue" title 'R:CPU0 - W:CPU53',\
	  'amd/default_socket/final-amd-default-evict-0-54.csv' ps 2 pt 6 lw 3 lc rgb "violet" title 'R:CPU0 - W:CPU54',\
	  'amd/default_socket/final-amd-default-evict-0-55.csv' ps 2 pt 6 lw 3 lc rgb "black" title 'R:CPU0 - W:CPU55',\
	  'amd/default_socket/final-amd-default-evict-0-56.csv' ps 2 pt 7 lw 3 lc rgb "red" title 'R:CPU0 - W:CPU56',\
	  'amd/default_socket/final-amd-default-evict-0-57.csv' ps 2 pt 7 lw 3 lc rgb "orange" title 'R:CPU0 - W:CPU57',\
	  'amd/default_socket/final-amd-default-evict-0-58.csv' ps 2 pt 7 lw 3 lc rgb "yellow" title 'R:CPU0 - W:CPU58',\
	  'amd/default_socket/final-amd-default-evict-0-59.csv' ps 2 pt 7 lw 3 lc rgb "green" title 'R:CPU0 - W:CPU59',\
	  'amd/default_socket/final-amd-default-evict-0-60.csv' ps 2 pt 8 lw 3 lc rgb "cyan" title 'R:CPU0 - W:CPU60',\
	  'amd/default_socket/final-amd-default-evict-0-61.csv' ps 2 pt 8 lw 3 lc rgb "blue" title 'R:CPU0 - W:CPU61',\
	  'amd/default_socket/final-amd-default-evict-0-62.csv' ps 2 pt 8 lw 3 lc rgb "violet" title 'R:CPU0 - W:CPU62',\
	  'amd/default_socket/final-amd-default-evict-0-63.csv' ps 2 pt 8 lw 3 lc rgb "black" title 'R:CPU0 - W:CPU63'





