#gnuplotfile

set title "Mesure des bandes passantes avec sOMP-BWB - Intel Xeon Gold 6240\n Paire unique lecteur/écrivain - Socket 1"
set xlabel "Données (Bytes)"
set ylabel "Bande passante (GBps)"

set yrange[1:100]

set logscale
set key top right vertical maxrows 16
set style data points
set terminal qt size 1000,800 font "Helvetica,22"
set key font ",17"

#plot 'intel/default_socket/final-intel-default-evict-0-0.csv' ps 2 pt 1 lw 3 lc rgb  "red" title 'R:CPU0 - W:CPU0',\
#     'intel/default_socket/final-intel-default-evict-0-01.csv' ps 2 pt 1 lw 3 lc rgb "orange" title 'R:CPU0 - W:CPU1',\
#     'intel/default_socket/final-intel-default-evict-0-02.csv' ps 2 pt 1 lw 3 lc rgb "yellow" title 'R:CPU0 - W:CPU2',\
#     'intel/default_socket/final-intel-default-evict-0-03.csv' ps 2 pt 1 lw 3 lc rgb "green" title 'R:CPU0 - W:CPU3',\
#     'intel/default_socket/final-intel-default-evict-0-04.csv' ps 2 pt 1 lw 3 lc rgb "cyan" title 'R:CPU0 - W:CPU4',\
#     'intel/default_socket/final-intel-default-evict-0-05.csv' ps 2 pt 1 lw 3 lc rgb "blue" title 'R:CPU0 - W:CPU5',\
#     'intel/default_socket/final-intel-default-evict-0-06.csv' ps 2 pt 1 lw 3 lc rgb "violet" title 'R:CPU0 - W:CPU6',\
#     'intel/default_socket/final-intel-default-evict-0-07.csv' ps 2 pt 1 lw 3 lc rgb "brown" title 'R:CPU0 - W:CPU7',\
#	  'intel/default_socket/final-intel-default-evict-0-08.csv' ps 2 pt 1 lw 3 lc rgb "grey" title 'R:CPU0 - W:CPU8',\
#	  'intel/default_socket/final-intel-default-evict-0-09.csv' ps 2 pt 1 lw 3 lc rgb "pink" title 'R:CPU0 - W:CPU9',\
#	  'intel/default_socket/final-intel-default-evict-0-10.csv' ps 2 pt 1 lw 3 lc rgb "skyblue" title 'R:CPU0 - W:CPU10',\
#	  'intel/default_socket/final-intel-default-evict-0-11.csv' ps 2 pt 1 lw 3 lc rgb "olive" title 'R:CPU0 - W:CPU11',\
#	  'intel/default_socket/final-intel-default-evict-0-12.csv' ps 2 pt 1 lw 3 lc rgb "gold" title 'R:CPU0 - W:CPU12',\
#	  'intel/default_socket/final-intel-default-evict-0-13.csv' ps 2 pt 1 lw 3 lc rgb "turquoise" title 'R:CPU0 - W:CPU13',\
#	  'intel/default_socket/final-intel-default-evict-0-14.csv' ps 2 pt 1 lw 3 lc rgb "purple" title 'R:CPU0 - W:CPU14',\
#	  'intel/default_socket/final-intel-default-evict-0-15.csv' ps 2 pt 1 lw 3 lc rgb "dark-green" title 'R:CPU0 - W:CPU15',\
#     'intel/default_socket/final-intel-default-evict-0-16.csv' ps 2 pt 1 lw 3 lc rgb "salmon" title 'R:CPU0 - W:CPU16',\
#	  'intel/default_socket/final-intel-default-evict-0-17.csv' ps 2 pt 1 lw 3 lc rgb "black" title 'R:CPU0 - W:CPU17'

plot 'intel/default_socket/final-intel-default-evict-0-18.csv' ps 2 pt 2 lw 3 lc rgb "red"         title 'R:CPU0 - W:CPU18',\
	  'intel/default_socket/final-intel-default-evict-0-19.csv' ps 2 pt 2 lw 3 lc rgb "orange"      title 'R:CPU0 - W:CPU19',\
	  'intel/default_socket/final-intel-default-evict-0-20.csv' ps 2 pt 2 lw 3 lc rgb "yellow"      title 'R:CPU0 - W:CPU20',\
	  'intel/default_socket/final-intel-default-evict-0-21.csv' ps 2 pt 2 lw 3 lc rgb "green"       title 'R:CPU0 - W:CPU21',\
	  'intel/default_socket/final-intel-default-evict-0-22.csv' ps 2 pt 2 lw 3 lc rgb "cyan"        title 'R:CPU0 - W:CPU22',\
	  'intel/default_socket/final-intel-default-evict-0-23.csv' ps 2 pt 2 lw 3 lc rgb "blue"        title 'R:CPU0 - W:CPU23',\
     'intel/default_socket/final-intel-default-evict-0-24.csv' ps 2 pt 2 lw 3 lc rgb "violet"      title 'R:CPU0 - W:CPU24',\
	  'intel/default_socket/final-intel-default-evict-0-25.csv' ps 2 pt 2 lw 3 lc rgb "brown"       title 'R:CPU0 - W:CPU25',\
	  'intel/default_socket/final-intel-default-evict-0-26.csv' ps 2 pt 2 lw 3 lc rgb  "grey"       title 'R:CPU0 - W:CPU26',\
	  'intel/default_socket/final-intel-default-evict-0-27.csv' ps 2 pt 2 lw 3 lc rgb  "pink"       title 'R:CPU0 - W:CPU27',\
	  'intel/default_socket/final-intel-default-evict-0-28.csv' ps 2 pt 2 lw 3 lc rgb  "skyblue"    title 'R:CPU0 - W:CPU28',\
	  'intel/default_socket/final-intel-default-evict-0-29.csv' ps 2 pt 2 lw 3 lc rgb  "olive"      title 'R:CPU0 - W:CPU29',\
	  'intel/default_socket/final-intel-default-evict-0-30.csv' ps 2 pt 2 lw 3 lc rgb  "gold"       title 'R:CPU0 - W:CPU30',\
	  'intel/default_socket/final-intel-default-evict-0-31.csv' ps 2 pt 2 lw 3 lc rgb  "turquoise"  title 'R:CPU0 - W:CPU31',\
     'intel/default_socket/final-intel-default-evict-0-32.csv' ps 2 pt 2 lw 3 lc rgb  "purple"     title 'R:CPU0 - W:CPU32',\
     'intel/default_socket/final-intel-default-evict-0-33.csv' ps 2 pt 2 lw 3 lc rgb  "dark-green" title 'R:CPU0 - W:CPU33',\
     'intel/default_socket/final-intel-default-evict-0-34.csv' ps 2 pt 2 lw 3 lc rgb "salmon"      title 'R:CPU0 - W:CPU34',\
     'intel/default_socket/final-intel-default-evict-0-35.csv' ps 2 pt 2 lw 3 lc rgb  "black"      title 'R:CPU0 - W:CPU35'

