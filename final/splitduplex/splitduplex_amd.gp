#gnuplotfile

set title "Mesure des bandes passantes avec sOMP-BWB - AMD EPYC 7452\n ".ARG1." lecteurs sur la die 0 - ".ARG1." écrivains sur la die 1"
set xlabel "Données (Bytes)"
set ylabel "Bande passante (GBps)"


set logscale x
set yrange[1:40]

set key top right vertical maxrows 16
set style data points
set terminal qt size 1000,800 font "Helvetica,22"
set key font ",17"

if (ARG1 eq "1"){
	plot 'amd/splitduplex/final-amd-misc-evict-splitduplex-1.csv' ps 2 pt 1 lw 3 lc rgb "red" title 'R:CPU0 - W:CPU4'
}
if (ARG1 eq "2"){
plot 'amd/splitduplex/final-amd-misc-evict-splitduplex-2.csv' using 1:2 ps 2 pt 1 lw 3 lc rgb "red" title 'R:CPU0 - W:CPU4',\
	  'amd/splitduplex/final-amd-misc-evict-splitduplex-2.csv' using 1:3 ps 2 pt 1 lw 3 lc rgb "orange" title 'R:CPU1 - W:CPU5',\
	  'amd/splitduplex/final-amd-misc-evict-splitduplex-2-sum.csv' using 1:2 ps 2 pt 2 lw 3 lc rgb "green" title 'Somme des bandes passantes'
}
if (ARG1 eq "3"){
plot 'amd/splitduplex/final-amd-misc-evict-splitduplex-3.csv' using 1:2 ps 2 pt 1 lw 3 lc rgb "red" title 'R:CPU0 - W:CPU4',\
	  'amd/splitduplex/final-amd-misc-evict-splitduplex-3.csv' using 1:3 ps 2 pt 1 lw 3 lc rgb "orange" title 'R:CPU1 - W:CPU5',\
	  'amd/splitduplex/final-amd-misc-evict-splitduplex-3.csv' using 1:4 ps 2 pt 1 lw 3 lc rgb "cyan" title 'R:CPU2 - W:CPU6',\
	  'amd/splitduplex/final-amd-misc-evict-splitduplex-3-sum.csv' using 1:2 ps 2 pt 2 lw 3 lc rgb "green" title 'Somme des bandes passantes'
}
if (ARG1 eq "4"){
plot 'amd/splitduplex/final-amd-misc-evict-splitduplex-4.csv' using 1:2 ps 2 pt 1 lw 3 lc rgb "red" title 'R:CPU0 - W:CPU4',\
	  'amd/splitduplex/final-amd-misc-evict-splitduplex-4.csv' using 1:3 ps 2 pt 1 lw 3 lc rgb "orange" title 'R:CPU1 - W:CPU5',\
	  'amd/splitduplex/final-amd-misc-evict-splitduplex-4.csv' using 1:4 ps 2 pt 1 lw 3 lc rgb "cyan" title 'R:CPU2 - W:CPU6',\
	  'amd/splitduplex/final-amd-misc-evict-splitduplex-4.csv' using 1:5 ps 2 pt 1 lw 3 lc rgb "blue" title 'R:CPU3 - W:CPU7',\
	  'amd/splitduplex/final-amd-misc-evict-splitduplex-4-sum.csv' using 1:2 ps 2 pt 2 lw 3 lc rgb "green" title 'Somme des bandes passantes'
}
