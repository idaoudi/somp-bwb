ifeq ($(shell pkg-config --libs hwloc 2> /dev/null),)
	$(error HWLoc not found. Please check paths.)
endif

CC = g++
CFLAGS = -std=c++11 -Wall -O3 -march=native $(shell pkg-config --cflags hwloc)
LIBS = -lpthread $(shell pkg-config --libs hwloc)

DIR=src
MAIN = main
TARGET = somp_bwb

all: $(TARGET)

$(TARGET): $(DIR)/$(MAIN).cpp $(wildcard src/headers/*.hpp)
	$(CC) $(CFLAGS) -o $(TARGET) $(DIR)/$(MAIN).cpp $(LIBS)

clean:
	$(RM) $(TARGET)


