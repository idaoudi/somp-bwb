**sOMP BandWidth Benchmark**

**Usage:**

- make

- ./somp_bwb -m _[bandwidth mode]_ -c _[reading CPUs]_ -d _[writing CPUs]_

**Bandwidth modes:**

- "**cross**" mode: readers are reading SIMULTANEOUSLY from different writers
- "**allr**" mode: a unique reader is reading from mutliple writers SEQUENTIALLY

**Reading and writing CPUs:**

- using LOGICAL index only (see HWLoc documentation for further details)
- multiple CPUs have to be comma separated

**Example** (machine with 2 sockets and 10 cores per socket):

- ./somp_bwb -m cross -c 0,9 -d 10,19 ---> CPUs 0 and 9 (resp.) will read in the same time data created by CPUs 10 and 19 (resp.)
- ./somp_bwb -m allr -c 0 -d 10,11,12 ---> CPU 0 reads data created by CPU 10, then CPU 11, and finally CPU 12
