#!/bin/bash

python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-1-case0.csv 1 0 0 1 2 3 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-2-case0.csv 2 0 0 1 2 3 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-3-case0.csv 3 0 0 1 2 3 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-4-case0.csv 4 0 0 1 2 3 0 1 2 3

python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-1-case1.csv 1 1 4 5 6 7 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-2-case1.csv 2 1 4 5 6 7 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-3-case1.csv 3 1 4 5 6 7 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-4-case1.csv 4 1 4 5 6 7 0 1 2 3

python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-1-case2.csv 1 2 8 9 10 11 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-2-case2.csv 2 2 8 9 10 11 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-3-case2.csv 3 2 8 9 10 11 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-4-case2.csv 4 2 8 9 10 11 0 1 2 3

python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-1-case3.csv 1 3 12 13 14 15 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-2-case3.csv 2 3 12 13 14 15 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-3-case3.csv 3 3 12 13 14 15 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-4-case3.csv 4 3 12 13 14 15 0 1 2 3

python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-1-case4.csv 1 4 16 17 18 19 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-2-case4.csv 2 4 16 17 18 19 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-3-case4.csv 3 4 16 17 18 19 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-4-case4.csv 4 4 16 17 18 19 0 1 2 3

python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-1-case5.csv 1 5 20 21 22 23 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-2-case5.csv 2 5 20 21 22 23 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-3-case5.csv 3 5 20 21 22 23 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-4-case5.csv 4 5 20 21 22 23 0 1 2 3

python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-1-case6.csv 1 6 24 25 26 27 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-2-case6.csv 2 6 24 25 26 27 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-3-case6.csv 3 6 24 25 26 27 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-4-case6.csv 4 6 24 25 26 27 0 1 2 3

python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-1-case7.csv 1 7 28 29 30 31 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-2-case7.csv 2 7 28 29 30 31 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-3-case7.csv 3 7 28 29 30 31 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-4-case7.csv 4 7 28 29 30 31 0 1 2 3

python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-1-case8.csv 1 8 32 33 34 35 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-2-case8.csv 2 8 32 33 34 35 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-3-case8.csv 3 8 32 33 34 35 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-4-case8.csv 4 8 32 33 34 35 0 1 2 3

python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-1-case15.csv 1 15 60 61 62 63 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-2-case15.csv 2 15 60 61 62 63 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-3-case15.csv 3 15 60 61 62 63 0 1 2 3
python3.9 multiplot.py amd results/amd/misc_new-nrnw/results-amd-misc-evict-new-nrnw-4-case15.csv 4 15 60 61 62 63 0 1 2 3







