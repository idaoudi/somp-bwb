import sys
import matplotlib.pyplot as plt
import numpy as np

f1 = sys.argv[1]
f2 = sys.argv[2]
f3 = sys.argv[3]
f4 = sys.argv[4]
f5 = sys.argv[5]
f6 = sys.argv[6]

data1 = np.genfromtxt(f1, delimiter=',', names=['nth', 'time'], loose=False, invalid_raise=False)
data2 = np.genfromtxt(f2, delimiter=',', names=['nth', 'time'], loose=False, invalid_raise=False)
data3 = np.genfromtxt(f3, delimiter=',', names=['nth', 'time'], loose=False, invalid_raise=False)
data4 = np.genfromtxt(f4, delimiter=',', names=['nth', 'time'], loose=False, invalid_raise=False)
data5 = np.genfromtxt(f5, delimiter=',', names=['nth', 'time'], loose=False, invalid_raise=False)
data6 = np.genfromtxt(f6, delimiter=',', names=['nth', 'time'], loose=False, invalid_raise=False)

plt.rcParams.update({'font.size': 12})
fig = plt.figure(figsize=(15,10))
ax1 = fig.add_subplot(111)

ax1.plot(data1['nth'], data1['time'], 'x-', label='Witness 1', color='red')
ax1.plot(data2['nth'], data2['time'], 'x-', label='Witness 2', color='red')
ax1.plot(data3['nth'], data3['time'], 'x-', label='Witness 3', color='red')
ax1.plot(data4['nth'], data4['time'], '.-', label='With BWB 1', color='green')
ax1.plot(data5['nth'], data5['time'], '.-', label='With BWB 2', color='green')
ax1.plot(data6['nth'], data6['time'], '.-', label='With BWB 3', color='green')

ax1.legend(ncol=1, loc='upper right')
ax1.set_xlabel('Number of cores')
ax1.set_ylabel('Time (s)')
filename = "comp-bw-multi-32.eps"
plt.savefig(filename, dpi=1200, format='eps')
plt.show()


