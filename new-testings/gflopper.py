import sys
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import numpy as np

def error(a,b):
    return 100*(a-b)/a 

def comp_qr(a,n):
    return 1e-9*(4*n*n*n/3)/a

def comp_chol(a,n):
    return 1e-9*((n*n*n/3) + (2*n*n/3))/a

alg = sys.argv[1]
f1 = sys.argv[2] #Native
f2 = sys.argv[3] #Simulation comm
f3 = sys.argv[4] #Simulation commcache
f4 = sys.argv[5] #Simulation gemm/task die
m = sys.argv[5]
m = int(m)

plt.rcParams.update({'font.size': 25})
fig = plt.figure(figsize=(15,10)) #FIXME
ax1 = fig.add_subplot(111)
colors = iter(cm.rainbow(np.linspace(0,1,3)))

natives      = np.genfromtxt(f1, delimiter=',', names=['threads', 'nat'], loose=False, invalid_raise=False)
#natives      = np.genfromtxt(f1, delimiter=' ', names=['m', 'b', 'threads', 'nat'], loose=False, invalid_raise=False)
simulated_cc = np.genfromtxt(f2, delimiter=' ', names=['m2', 'b2', 'threads2', 'simcc', 'a2', 'b2', 'c2', 'd2'], loose=False, invalid_raise=False)
simulated_c  = np.genfromtxt(f3, delimiter=' ', names=['m1', 'b1', 'threads1', 'simc', 'a1', 'b1', 'c1', 'd1'], loose=False, invalid_raise=False)
simulated_g  = np.genfromtxt(f4, delimiter=' ', names=['m3', 'b3', 'threads3', 'simg', 'a3', 'b3', 'c3', 'd3'], loose=False, invalid_raise=False)

color = next(colors)
g = next(colors)
if (alg == "cholesky"):
    ax1.plot(natives['threads'], comp_chol(simulated_g['simg'],m), 'v-', label='TASK model', color=next(colors))
    ax1.plot(natives['threads'], comp_chol(simulated_cc['simcc'],m), '*-', label='COMM+CACHE model', color=g, markersize=15, linewidth=4)
    ax1.plot(natives['threads'], comp_chol(simulated_c['simc'],m), 'v-', label='COMM model', color=g, markersize=15, linewidth=4)
    ax1.plot(natives['threads'], comp_chol(natives['nat'],m), 'o-', color='black', markersize=15, linewidth=4)
    #ax1.plot(natives['threads'], comp_chol(natives['nat'],m), 'o-', label='Native', color='black', markersize=15, linewidth=4)
elif (alg == "qr"):
    #ax1.plot(natives['threads'], comp_qr(simulated_g['simg'],m), 'v-', label='Task-die model', color=next(colors))
    ax1.plot(natives['threads'], comp_qr(natives['nat'],m), '.-', label='Native', color=next(colors), markersize=15, linewidth=4)
    ax1.plot(natives['threads'], comp_qr(simulated_cc['simcc'],m), 'o-', label='COMM+CACHE model', color=next(colors), markersize=15, linewidth=4)
    ax1.plot(natives['threads'], comp_qr(simulated_c['simc'],m), '*-', label='COMM+CACHE model + SCHED', color=next(colors), markersize=15, linewidth=4)

#for i in range(-50,50,5):
#    plt.axhline(y=i, xmin=-50, xmax=50, linewidth=0.1, color='black') 
#plt.ylim([0,1200.0])
#ax1.legend(ncol=1, loc='upper left')
ax1.set_xlabel('Number of cores')
ax1.set_ylabel('Performance (GFlops)')
filename="icppgflops-diablo04-"+alg+"-"+str(m)+"-512-natonly.eps"
plt.savefig(filename, dpi=1200, format='eps')
plt.show()



