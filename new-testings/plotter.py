import sys
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import numpy as np

def error(a,b):
    return 100*(a-b)/a 

arch = sys.argv[1]
f1 = sys.argv[2] #Native
f2 = sys.argv[3] #Simulation comm
f3 = sys.argv[4] #Simulation commcache
f4 = sys.argv[5] #Simulation gemm/task die
#f5 = sys.argv[6] #Simulation gemm/task die
m = sys.argv[7]

plt.rcParams.update({'font.size': 12})
fig = plt.figure(figsize=(15,10)) #FIXME
ax1 = fig.add_subplot(111)
colors = iter(cm.rainbow(np.linspace(0,1,3)))

natives      = np.genfromtxt(f1, delimiter=',', names=['threads', 'nat'], loose=False, invalid_raise=False)
#natives      = np.genfromtxt(f1, delimiter=' ', names=['m', 'b', 'threads', 'nat'], loose=False, invalid_raise=False)
simulated_c  = np.genfromtxt(f2, delimiter=' ', names=['m1', 'b1', 'threads1', 'simc', 'a1', 'b1', 'c1', 'd1'], loose=False, invalid_raise=False)
simulated_cc = np.genfromtxt(f3, delimiter=' ', names=['m2', 'b2', 'threads2', 'simcc', 'a2', 'b2', 'c2', 'd2'], loose=False, invalid_raise=False)
simulated_g  = np.genfromtxt(f4, delimiter=' ', names=['m3', 'b3', 'threads3', 'simg', 'a3', 'b3', 'c3', 'd3'], loose=False, invalid_raise=False)
#simulated_g1  = np.genfromtxt(f5, delimiter=' ', names=['m4', 'b4', 'threads4', 'simg4', 'a4', 'b4', 'c4', 'd4'], loose=False, invalid_raise=False)

#ax1.plot(natives['threads'], error(natives['nat'],simulated_g1['simg4']), 'v-', label='COMM+CACHE nocps model', color=next(colors))
ax1.plot(natives['threads'], error(natives['nat'],simulated_g['simg']), 'o-', label='TASK-die model', color=next(colors))
ax1.plot(natives['threads'], error(natives['nat'],simulated_cc['simcc']), '*-', label='COMM+CACHE model', color=next(colors))
ax1.plot(natives['threads'], error(natives['nat'],simulated_c['simc']), '.-', label='COMM model', color=next(colors))

for i in range(-50,50,5):
    plt.axhline(y=i, xmin=-50, xmax=50, linewidth=0.1, color='black') 
plt.ylim([-50.0,50.0])
ax1.legend(ncol=1, loc='upper right')
ax1.set_xlabel('Number of cores')
ax1.set_ylabel('Precision error (%)')
filename=arch+"-bora027-cholesky-govpowersave-intelpowersave-ratio0.6-measured-"+m+"-512-nocps.eps"
plt.savefig(filename, dpi=1200, format='eps')
plt.show()



